import { useEffect, useState } from 'react';
import './App.css';
import {useSpeechSynthesis} from "react-speech-kit";
import CountdownTimer from './lib/CountdownTimer/CountdownTimer';
import dayjs from 'dayjs';
import introSound from './assets/sound/Intro.wav';
import outroSound from './assets/sound/Outro.wav';
import { wait } from '@testing-library/user-event/dist/utils';

const defaultRemainingTime = {
  seconds: '00',
  minutes: '00',
  hours: '00',
  days: '00'
}

function App() {
  const [remainingTime, setRemainingTime] = useState(defaultRemainingTime);
  const[value, setValue] = useState('Hello Testing Web Speech API');
  const {speak, voices,speaking} = useSpeechSynthesis();
  const [firstMount, setFirstMount] = useState(true);
 

  const changeText = () => {
    const text = "To all BACK members, BACK secretariat, and BACK TWG! please be informed, that we will have a pre-bid conference at 2 o'clock this afternoon for the construction of Multi-Purpose Building at baranggay Manuok, Gubat, Sorsogon with contract ID number 22FL0098.";
    setValue( 'Attention! ' + text + ' ' +text + ' ' +'Thank you!');
  }

  function playAllert(audio){
    return new Promise(res => {
      audio.play()
      audio.onended = res
    })
  }

  function playAnnouncement(announcement){
    return new Promise(res => {
      speak({text:announcement, voice: voices[97]});
      while(speaking === true){
        console.log('hahaha'); 
      }
    })
  }

  async function playIntro(){
    const audio = new Audio(introSound);
    console.log('Intro')
    await playAllert(audio);
    console.log('Announcement start')
    await playAnnouncement(value)
    
    await(speaking === false).then(
      console.log('Announcement end'),
    new Audio(outroSound).play()
    )
    
  }
  
  function playOutro(){
    new Audio(outroSound).play();
  }

  function announce(){
    playIntro();
    // speak({text:value, voice: voices[96]});
    // playIntro();
  }

  useEffect(() => {
    if(firstMount === true){
      setFirstMount(false);
    }else{
      if(speaking === false){
        console.log('outro')
        playOutro();
      }
    }
  }, [speaking])

  return (
    <div className="App">
      <button onClick={changeText}>To BAC</button>
      <button onClick={announce}>Speak</button>

      <CountdownTimer CountdownTimeStampMS={dayjs('2022-10-25 10:30')} remainingTime={remainingTime} setRemainingTime={setRemainingTime}/>

    

    </div>
  );
}

export default App;
