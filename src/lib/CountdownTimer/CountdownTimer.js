import { useEffect, useState } from "react";
import { Alert, Container } from "react-bootstrap";
import { getRemainingTimeUntilMSTimeStamp } from "./Utils/CountdownTimerUtils";



const CountdownTimer = ({CountdownTimeStampMS, remainingTime, setRemainingTime}) => {

    const [runOnce, setRunOnce] = useState(true);

    useEffect(() => {
        const intervalId = setInterval(() => {
            updateRemainingTime(CountdownTimeStampMS);
            // if(remainingTime.seconds === '00' && runOnce === true){
            //     console.log('Hahaha');
            //     setRunOnce(false);
            // }
        }, 1000)

        return () => clearInterval(intervalId);
    },[CountdownTimeStampMS, remainingTime])

    function updateRemainingTime(countdown){
        setRemainingTime(getRemainingTimeUntilMSTimeStamp(countdown));
       
    }

    

    return (
        <Container>
            {
                remainingTime.seconds === 0 && remainingTime.minutes === 0 ? 'Hahaha' : 'Hohoho'
            }
            <Alert>
                <Alert.Heading style={{ marginBottom: '0' }}>Opening of Bids - 22FL0098</Alert.Heading>
                <p style={{ marginBottom: '0' }}><b>Remaining Time</b></p>
                <p style={{ marginBottom: '0' }}>day : <b>{remainingTime.days}</b> | hours : <b>{remainingTime.hours}</b> | minutes : <b>{remainingTime.minutes}</b> | seconds : <b>{remainingTime.seconds}</b></p>
            </Alert>
        </Container>
    )
}

export default CountdownTimer;